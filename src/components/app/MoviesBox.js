import React from 'react';

class MoviesBox extends React.Component {

  render() {
    return (
    	<div className="tableglobal">
    	<table className="table table-condensed">
				<thead>
					<tr>
	    			<th className="td">Title</th>
	    			<th className="td">Genre</th> 
	    			<th className="td">Description</th>
	    			<th className="td">Poster</th>
	 				</tr>
				</thead>
 				<tbody>
	 				 <tr>
	    			<th>{this.props.movie['Title']}</th>
	    			<th>{this.props.movie['Genre']}</th>
	    			<th>{this.props.movie['Plot']}</th>
	    			<th><img src={this.props.movie['Poster']} /></th>
	  			</tr>
  			</tbody>
			</table>
			</div>

    );
  }
}

export default MoviesBox;

