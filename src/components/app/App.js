import Movies from './MoviesBox';
import React from 'react'

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
     movie: '',
     movieDetail:[{
     }]
    };
  }

  loadMovie() {   
    $.ajax({
      url:'http://www.omdbapi.com/?t='+this.state.movie+'&y=&plot=full&r=json',    
      dataType: 'json',
      method:"get",        
      success: (data) => { 
        this.setState({
          movieDetail:data
        })
      },
      error: (xhr, status, err) => {  
        console.error(this.props.url, status, err.toString());
      }   
    }); 
  } 

  gettingSearch(e){
    const newMovie = e.target.value;
    this.setState({
      movie: newMovie
    });
  }



  render() {
    return (
      <div className="container">
        <div className="input-group">
          <span className="input-group-btn">
          <button className = "btn btn-primary" onClick={this.loadMovie.bind(this)}>Search</button>
          </span>
          <input type="text" className="form-control search" onChange={this.gettingSearch.bind(this)} placeholder="Search for..."/>
        </div>
        <Movies
          movie= {this.state.movieDetail}
        />
      </div>
    );
  }
}

export default App;
